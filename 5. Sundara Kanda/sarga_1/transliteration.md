transliteration
<p>tatō rāvaṇanītāyāḥ sītāyāḥ śatrukarśanaḥ |<br />
iyēṣa padamanvēṣṭuṁ cāraṇācaritē pathi || 1 ||</p>
<p>duṣkaraṁ niṣpratidvandvaṁ cikīrṣankarma vānaraḥ |<br />
samudagraśirōgrīvō gavāṁ patirivā:&#8217;:&#8217;babhau || 2 ||</p>
<p>atha vaiḍūryavarṇēṣu śādvalēṣu mahābalaḥ |<br />
dhīraḥ salilakalpēṣu vicacāra yathāsukham || 3 ||</p>
<p>dvijānvitrāsayandhīmānurasā pādapānharan |<br />
mr̥gāṁśca subāhūnnighnanpravr̥ddha iva kēsarī || 4 ||</p>
<p>nīlalōhitamāñjiṣṭhapatravarṇaiḥ sitāsitaiḥ |<br />
svabhāvavihitaiścitrairdhātubhiḥ samalaṅkr̥tam || 5 ||</p>
<p>kāmarūpibhirāviṣṭamabhīkṣṇaṁ saparicchadaiḥ |<br />
yakṣakinnaragandharvairdēvakalpaiśca pannagaiḥ || 6 ||</p>
<p>sa tasya girivaryasya talē nāgavarāyutē |<br />
tiṣṭhankapivarastatra hradē nāga ivābabhau || 7 ||</p>
<p>sa sūryāya mahēndrāya pavanāya svayambhuvē |<br />
bhūtēbhyaścāñjaliṁ kr̥tvā cakāra gamanē matim || 8 ||</p>
<p>añjaliṁ prāṅmukhaḥ kr̥tvā pavanāyātmayōnayē |<br />
tatō:&#8217;bhivavr̥dhē gantuṁ dakṣiṇō dakṣiṇāṁ diśam || 9 ||</p>
<p>plavaṅgapravarairdr̥ṣṭaḥ plavanē kr̥taniścayaḥ |<br />
vavr̥dhē rāmavr̥ddhyarthaṁ samudra iva parvasu || 10 ||</p>
<p>niṣpramāṇaśarīraḥ san lilaṅghayiṣurarṇavam |<br />
bāhubhyāṁ pīḍayāmāsa caraṇābhyāṁ ca parvatam || 11 ||</p>
<p>sa cacālācalaścāpi muhūrtaṁ kapipīḍitaḥ |<br />
tarūṇāṁ puṣpitāgrāṇāṁ sarvaṁ puṣpamaśātayat || 12 ||</p>
<p>tēna pādapamuktēna puṣpaughēṇa sugandhinā |<br />
sarvataḥ saṁvr̥taḥ śailō babhau puṣpamayō yathā || 13 ||</p>
<p>tēna cōttamavīryēṇa pīḍyamānaḥ sa parvataḥ |<br />
salilaṁ samprasusrāva madaṁ matta iva dvipaḥ || 14 ||</p>
<p>pīḍyamānastu balinā mahēndrastēna parvataḥ |<br />
rītīrnirvartayāmāsa kāñcanāñjanarājatīḥ || 15 ||</p>
<p>mumōca ca śilāḥ śailō viśālāḥ samanaḥ śilāḥ |<br />
madhyamēnārciṣā juṣṭō dhūmarājīrivānalaḥ || 16 ||</p>
<p>giriṇā pīḍyamānēna pīḍyamānāni sarvataḥ |<br />
guhāviṣṭāni bhūtāni vinēdurvikr̥taiḥ svaraiḥ || 17 ||</p>
<p>sa mahāsattvasannādaḥ śailapīḍānimittajaḥ |<br />
pr̥thivīṁ pūrayāmāsa diśaścōpavanāni ca || 18 ||</p>
<p>śirōbhiḥ pr̥thubhiḥ sarpā vyaktasvastikalakṣaṇaiḥ |<br />
vamantaḥ pāvakaṁ ghōraṁ dadaṁśurdaśanaiḥ śilāḥ || 19 ||</p>
<p>tāstadā saviṣairdaṣṭāḥ kupitaistairmahāśilāḥ |<br />
jajjvaluḥ pāvakōddīptā bibhiduśca sahasradhā || 20 ||</p>
<p>yāni cauṣadhajālāni tasmin jātāni parvatē |<br />
viṣaghnānyapi nāgānāṁ na śēkuḥ śamituṁ viṣam || 21 ||</p>
<p>bhidyatē:&#8217;yaṁ girirbhūtairiti mattvā tapasvinaḥ |<br />
trastā vidyādharāstasmādutpētuḥ strīgaṇaiḥ saha || 22 ||</p>
<p>pānabhūmigataṁ hitvā haimamāsavabhājanam |<br />
pātrāṇi ca mahārhāṇi karakāṁśca hiraṇmayān || 23 ||</p>
<p>lēhyānuccāvacān bhakṣyān māṁsāni vividhāni ca |<br />
ārṣabhāṇi ca carmāṇi khaḍgāṁśca kanakatsarūn || 24 ||</p>
<p>kr̥takaṇṭhaguṇāḥ kṣībā raktamālyānulēpanāḥ |<br />
raktākṣāḥ puṣkarākṣāśca gaganaṁ pratipēdirē || 25 ||</p>
<p>hāranūpurakēyūrapārihāryadharāḥ striyaḥ |<br />
vismitāḥ sasmitāstasthurākāśē ramaṇaiḥ saha || 26 ||</p>
<p>darśayantō mahāvidyāṁ vidyādharamaharṣayaḥ |<br />
sahitāstasthurākāśē vīkṣāñcakruśca parvatam || 27 ||</p>
<p>śuśruvuśca tadā śabdamr̥ṣīṇāṁ bhāvitātmanām |<br />
cāraṇānāṁ ca siddhānāṁ sthitānāṁ vimalē:&#8217;mbarē || 28 ||</p>
<p>ēṣa parvatasaṅkāśō hanūmānmārutātmajaḥ |<br />
titīrṣati mahāvēgaḥ samudraṁ makarālayam || 29 ||</p>
<p>rāmārthaṁ vānarārthaṁ ca cikīrṣankarma duṣkaram |<br />
samudrasya paraṁ pāraṁ duṣprāpaṁ prāptumicchati || 30 ||</p>
<p>iti vidyādharāḥ śrutvā vacastēṣāṁ mahātmanām |<br />
tamapramēyaṁ dadr̥śuḥ parvatē vānararṣabham || 31 ||</p>
<p>dudhuvē ca sa rōmāṇi cakampē cācalōpamaḥ |<br />
nanāda sumahānādaṁ sumahāniva tōyadaḥ || 32 ||</p>
<p>ānupūrvyēṇa vr̥ttaṁ ca lāṅgūlaṁ rōmabhiścitam |<br />
utpatiṣyanvicikṣēpa pakṣirāja ivōragam || 33 ||</p>
<p>tasya lāṅgūlamāviddhamāttavēgasya pr̥ṣṭhataḥ |<br />
dadr̥śē garuḍēnēva hriyamāṇō mahōragaḥ || 34 ||</p>
<p>bāhū saṁstambhayāmāsa mahāparighasannibhau |<br />
sasāda ca kapiḥ kaṭyāṁ caraṇau sañcukōca ca || 35 ||</p>
<p>saṁhr̥tya ca bhujau śrīmāṁstathaiva ca śirōdharām |<br />
tējaḥ sattvaṁ tathā vīryamāvivēśa sa vīryavān || 36 ||</p>
<p>mārgamālōkayandūrādūrdhvaṁ praṇihitēkṣaṇaḥ |<br />
rurōdha hr̥dayē prāṇānākāśamavalōkayan || 37 ||</p>
<p>padbhyāṁ dr̥ḍhamavasthānaṁ kr̥tvā sa kapikuñjaraḥ |<br />
nikuñcya karṇau hanumānutpatiṣyanmahābalaḥ || 38 ||</p>
<p>vānarānvānaraśrēṣṭha idaṁ vacanamabravīt |<br />
yathā rāghavanirmuktaḥ śaraḥ śvasanavikramaḥ || 39 ||</p>
<p>gacchēttadvadgamiṣyāmi laṅkāṁ rāvaṇapālitām |<br />
na hi drakṣyāmi yadi tāṁ laṅkāyāṁ janakātmajām || 40 ||</p>
<p>anēnaiva hi vēgēna gamiṣyāmi surālayam |<br />
yadi vā tridivē sītāṁ na drakṣyāmyakr̥taśramaḥ || 41 ||</p>
<p>baddhvā rākṣasarājānamānayiṣyāmi rāvaṇam |<br />
sarvathā kr̥takāryō:&#8217;hamēṣyāmi saha sītayā || 42 ||</p>
<p>ānayiṣyāmi vā laṅkāṁ samutpāṭya sarāvaṇām |<br />
ēvamuktvā tu hanūmānvānarānvānarōttamaḥ || 43 ||</p>
<p>utpapātātha vēgēna vēgavānavicārayan |<br />
suparṇamiva cātmānaṁ mēnē sa kapikuñjaraḥ || 44 ||</p>
<p>samutpatati tasmiṁstu vēgāttē nagarōhiṇaḥ |<br />
saṁhr̥tya viṭapānsarvānsamutpētuḥ samantataḥ || 45 ||</p>
<p>sa mattakōyaṣṭibakānpādapānpuṣpaśālinaḥ |<br />
udvahannūruvēgēna jagāma vimalē:&#8217;mbarē || 46 ||</p>
<p>ūruvēgōddhatā vr̥kṣā muhūrtaṁ kapimanvayuḥ |<br />
prasthitaṁ dīrghamadhvānaṁ svabandhumiva bāndhavāḥ || 47 ||</p>
<p>tamūruvēgōnmathitāḥ sālāścānyē nagōttamāḥ |<br />
anujagmurhanūmantaṁ sainyā iva mahīpatim || 48 ||</p>
<p>supuṣpitāgrairbahubhiḥ pādapairanvitaḥ kapiḥ |<br />
hanūmānparvatākārō babhūvādbhutadarśanaḥ || 49 ||</p>
<p>sāravantō:&#8217;tha yē vr̥kṣā nyamajjam̐llavaṇāmbhasi |<br />
bhayādiva mahēndrasya parvatā varuṇālayē || 50 ||</p>
<p>sa nānākusumaiḥ kīrṇaḥ kapiḥ sāṅkurakōrakaiḥ |<br />
śuśubhē mēghasaṅkāśaḥ khadyōtairiva parvataḥ || 51 ||</p>
<p>vimuktāstasya vēgēna muktvā puṣpāṇi tē drumāḥ |<br />
avaśīryanta salilē nivr̥ttāḥ suhr̥dō yathā || 52 ||</p>
<p>laghutvēnōpapannaṁ tadvicitraṁ sāgarē:&#8217;patat |<br />
drumāṇāṁ vividhaṁ puṣpaṁ kapivāyusamīritam || 53 ||</p>
<p>tārācitamivākāśaṁ prababhau sa mahārṇavaḥ |<br />
puṣpaughēṇānubaddhēna nānāvarṇēna vānaraḥ |<br />
babhau mēgha ivākāśē vidyudgaṇavibhūṣitaḥ || 54 ||</p>
<p>tasya vēgasamādhūtaiḥ puṣpaistōyamadr̥śyata |<br />
tārābhirabhirāmābhiruditābhirivāmbaram || 55 ||</p>
<p>tasyāmbaragatau bāhū dadr̥śātē prasāritau |<br />
parvatāgrādviniṣkrāntau pañcāsyāviva pannagau || 56 ||</p>
<p>pibanniva babhau śrīmān sōrmimālaṁ mahārṇavam | [cāpi]<br />
pipāsuriva cākāśaṁ dadr̥śē sa mahākapiḥ || 57 ||</p>
<p>tasya vidyutprabhākārē vāyumārgānusāriṇaḥ |<br />
nayanē viprakāśētē parvatasthāvivānalau || 58 ||</p>
<p>piṅgē piṅgākṣamukhyasya br̥hatī parimaṇḍalē |<br />
cakṣuṣī samprakāśētē candrasūryāvivōditau || 59 ||</p>
<p>mukhaṁ nāsikayā tasya tāmrayā tāmramābabhau |<br />
sandhyayā samabhispr̥ṣṭaṁ yathā sūryasya maṇḍalam || 60 || [tatsūrya] ||</p>
<p>lāṅgūlaṁ ca samāviddhaṁ plavamānasya śōbhatē |<br />
ambarē vāyuputrasya śakradhvaja ivōcchritaḥ || 61 ||</p>
<p>lāṅgūlacakrēṇa mahān śukladaṁṣṭrō:&#8217;nilātmajaḥ |<br />
vyarōcata mahāprājñaḥ parivēṣīva bhāskaraḥ || 62 ||</p>
<p>sphigdēśēnābhitāmrēṇa rarāja sa mahākapiḥ |<br />
mahatā dāritēnēva girirgairikadhātunā || 63 ||</p>
<p>tasya vānarasiṁhasya plavamānasya sāgaram |<br />
kakṣāntaragatō vāyurjīmūta iva garjati || 64 ||</p>
<p>khē yathā nipatantyulkā hyuttarāntādviniḥsr̥tā |<br />
dr̥śyatē sānubandhā ca tathā sa kapikuñjaraḥ || 65 ||</p>
<p>patatpataṅgasaṅkāśō vyāyataḥ śuśubhē kapiḥ |<br />
pravr̥ddha iva mātaṅgaḥ kakṣyayā badhyamānayā || 66 ||</p>
<p>upariṣṭāccharīrēṇa chāyayā cāvagāḍhayā |<br />
sāgarē mārutāviṣṭā naurivāsīttadā kapiḥ || 67 ||</p>
<p>yaṁ yaṁ dēśaṁ samudrasya jagāma sa mahākapiḥ |<br />
sa sa tasyōruvēgēna sōnmāda iva lakṣyatē || 68 ||</p>
<p>sāgarasyōrmijālānāmurasā śailavarṣmaṇā |<br />
abhighnaṁstu mahāvēgaḥ pupluvē sa mahākapiḥ || 69 ||</p>
<p>kapivātaśca balavānmēghavātaśca niḥsr̥taḥ |<br />
sāgaraṁ bhīmanirghōṣaṁ kampayāmāsaturbhr̥śam || 70 ||</p>
<p>vikarṣannūrmijālāni br̥hanti lavaṇāmbhasi |<br />
pupluvē kapiśārdūlō vikiranniva rōdasī || 71 ||</p>
<p>mērumandarasaṅkāśānuddhatānsa mahārṇavē |<br />
atikrāmanmahāvēgastaraṅgāngaṇayanniva || 72 ||</p>
<p>tasya vēgasamuddhūtaṁ jalaṁ sajaladaṁ tadā |<br />
ambarasthaṁ vibabhrāja śāradābhramivātatam || 73 ||</p>
<p>timinakrajhaṣāḥ kūrmā dr̥śyantē vivr̥tāstadā |<br />
vastrāpakarṣaṇēnēva śarīrāṇi śarīriṇām || 74 ||</p>
<p>plavamānaṁ samīkṣyātha bhujaṅgāḥ sāgarālayāḥ |<br />
vyōmni taṁ kapiśārdūlaṁ suparṇa iti mēnirē || 75 ||</p>
<p>daśayōjanavistīrṇā triṁśadyōjanamāyatā |<br />
chāyā vānarasiṁhasya jalē cārutarā:&#8217;bhavat || 76 ||</p>
<p>śvētābhraghanarājīva vāyuputrānugāminī |<br />
tasya sā śuśubhē chāyā vitatā lavaṇāmbhasi || 77 ||</p>
<p>śuśubhē sa mahātējā mahākāyō mahākapiḥ |<br />
vāyumārgē nirālambē pakṣavāniva parvataḥ || 78 ||</p>
<p>yēnāsau yāti balavānvēgēna kapikuñjaraḥ |<br />
tēna mārgēṇa sahasā drōṇīkr̥ta ivārṇavaḥ || 79 ||</p>
<p>āpātē pakṣisaṅghānāṁ pakṣirāja ivābabhau | [vrajan]<br />
hanūmānmēghajālāni prakarṣanmārutō yathā || 80 ||</p>
<p>pāṇḍurāruṇavarṇāni nīlamāñjiṣṭhakāni ca |<br />
kapinā:&#8217;:&#8217;kr̥ṣyamāṇāni mahābhrāṇi cakāśirē || 81 ||</p>
<p>praviśannabhrajālāni niṣpataṁśca punaḥ punaḥ |<br />
pracchannaśca prakāśaśca candramā iva lakṣyatē || 82 ||</p>
<p>plavamānaṁ tu taṁ dr̥ṣṭvā plavaṅgaṁ tvaritaṁ tadā |<br />
vavarṣuḥ puṣpavarṣāṇi dēvagandharvadānavāḥ || 83 ||</p>
<p>tatāpa na hi taṁ sūryaḥ plavantaṁ vānarōttamam |<br />
siṣēvē ca tadā vāyū rāmakāryārthasiddhayē || 84 ||</p>
<p>r̥ṣayastuṣṭuvuścainaṁ plavamānaṁ vihāyasā |<br />
jaguśca dēvagandharvāḥ praśaṁsantō mahaujasam || 85 ||</p>
<p>nāgāśca tuṣṭuvuryakṣā rakṣāṁsi vibudhāḥ khagāḥ |<br />
prēkṣya sarvē kapivaraṁ sahasā vigataklamam || 86 ||</p>
<p>tasmin plavagaśārdūlē plavamānē hanūmati |<br />
ikṣvākukulamānārthī cintayāmāsa sāgaraḥ || 87 ||</p>
<p>sāhāyyaṁ vānarēndrasya yadi nāhaṁ hanūmataḥ |<br />
kariṣyāmi bhaviṣyāmi sarvavācyō vivakṣatām || 88 ||</p>
<p>ahamikṣvākunāthēna sagarēṇa vivardhitaḥ |<br />
ikṣvākusacivaścāyaṁ nāvasīditumarhati || 89 ||</p>
<p>tathā mayā vidhātavyaṁ viśramēta yathā kapiḥ |<br />
śēṣaṁ ca mayi viśrāntaḥ sukhēnātipatiṣyati || 90 ||</p>
<p>iti kr̥tvā matiṁ sādhvīṁ samudraśchannamambhasi |<br />
hiraṇyanābhaṁ mainākamuvāca girisattamam || 91 ||</p>
<p>tvamihāsurasaṅghānāṁ pātālatalavāsinām |<br />
dēvarājñā giriśrēṣṭha parighaḥ sannivēśitaḥ || 92 ||</p>
<p>tvamēṣāṁ jātavīryāṇāṁ punarēvōtpatiṣyatām |<br />
pātālasyā:&#8217;pramēyasya dvāramāvr̥tya tiṣṭhasi || 99 ||</p>
<p>tiryagūrdhvamadhaścaiva śaktistē śaila vardhitum |<br />
tasmātsañcōdayāmi tvāmuttiṣṭha girisattama || 94 ||</p>
<p>sa ēṣa kapiśārdūlastvāmuparyēti vīryavān | [upaiṣyati]<br />
hanūmānrāmakāryārthaṁ bhīmakarmā khamāplutaḥ || 95 ||</p>
<p>asya sāhyaṁ mayā kāryamikṣvākukulavartinaḥ |<br />
mama hīkṣvākavaḥ pūjyāḥ paraṁ pūjyatamāstava || 96 ||</p>
<p>kuru sācivyamasmākaṁ na naḥ kāryamatikramēt |<br />
kartavyamakr̥taṁ kāryaṁ satāṁ manyumudīrayēt || 97 ||</p>
<p>salilādūrdhvamuttiṣṭha tiṣṭhatvēṣa kapistvayi |<br />
asmākamatithiścaiva pūjyaśca plavatāṁ varaḥ || 98 ||</p>
<p>cāmīkaramahānābha dēvagandharvasēvita |<br />
hanūmāṁstvayi viśrāntastataḥ śēṣaṁ gamiṣyati || 99 ||</p>
<p>kākutsthasyānr̥śaṁsyaṁ ca maithilyāśca vivāsanam |<br />
śramaṁ ca plavagēndrasya samīkṣyōtthātumarhasi || 100 ||</p>
<p>hiraṇyanābhō mainākō niśamya lavaṇāmbhasaḥ |<br />
utpapāta jalāttūrṇaṁ mahādrumalatāyutaḥ || 101 ||</p>
<p>sa sāgarajalaṁ bhittvā babhūvābhyutthitastadā |<br />
yathā jaladharaṁ bhittvā dīptaraśmirdivākaraḥ || 102 ||</p>
<p>sa mahātmā muhūrtēna parvataḥ salilāvr̥taḥ |<br />
darśayāmāsa śr̥ṅgāṇi sāgarēṇa niyōjitaḥ || 103 ||</p>
<p>śātakumbhamayaiḥ śr̥ṅgaiḥ sakinnaramahōragaiḥ | [ṁibhaiḥ]<br />
ādityōdayasaṅkāśairālikhadbhirivāmbaram || 104 ||</p>
<p>taptajāmbūnadaiḥ śr̥ṅgaiḥ parvatasya samutthitaiḥ |<br />
ākāśaṁ śastrasaṅkāśamabhavatkāñcanaprabham || 105 ||</p>
<p>jātarūpamayaiḥ śr̥ṅgairbhrājamānaiḥ svayamprabhaiḥ |<br />
ādityaśatasaṅkāśaḥ sō:&#8217;bhavadgirisattamaḥ || 106 ||</p>
<p>tamutthitamasaṅgēna hanumānagrataḥ sthitam |<br />
madhyē lavaṇatōyasya vighnō:&#8217;yamiti niścitaḥ || 107 ||</p>
<p>sa tamucchritamatyarthaṁ mahāvēgō mahākapiḥ |<br />
urasā pātayāmāsa jīmūtamiva mārutaḥ || 108 ||</p>
<p>sa tathā pātitastēna kapinā parvatōttamaḥ |<br />
buddhvā tasya kapērvēgaṁ jaharṣa ca nananda ca || 109 ||</p>
<p>tamākāśagataṁ vīramākāśē samupasthitaḥ |<br />
prītō hr̥ṣṭamanā vākyamabravītparvataḥ kapim || 110 ||</p>
<p>mānuṣaṁ dhārayanrūpamātmanaḥ śikharē sthitaḥ |<br />
duṣkaraṁ kr̥tavānkarma tvamidaṁ vānarōttama || 111 ||</p>
<p>nipatya mama śr̥ṅgēṣu viśramasva yathāsukham |<br />
rāghavasya kulē jātairudadhiḥ parivardhitaḥ || 112 ||</p>
<p>sa tvāṁ rāmahitē yuktaṁ pratyarcayati sāgaraḥ |<br />
kr̥tē ca pratikartavyamēṣa dharmaḥ sanātanaḥ || 113 || ||</p>
<p>sō:&#8217;yaṁ tvatpratikārārthī tvattaḥ sammānamarhati |<br />
tvannimittamanēnāhaṁ bahumānātpracōditaḥ || 114 ||</p>
<p>tiṣṭha tvaṁ hariśārdūla mayi viśramya gamyatām | [kapi]<br />
yōjanānāṁ śataṁ cāpi kapirēṣa samāplutaḥ || 115 ||</p>
<p>tava sānuṣu viśrāntaḥ śēṣaṁ prakramatāmiti |<br />
tadidaṁ gandhavatsvādu kandamūlaphalaṁ bahu || 116 ||</p>
<p>tadāsvādya hariśrēṣṭha viśrāntō:&#8217;nugamiṣyasi | [viśramya śvō]<br />
asmākamapi sambandhaḥ kapimukhya tvayā:&#8217;sti vai || 117 ||</p>
<p>prakhyātastriṣu lōkēṣu mahāguṇaparigrahaḥ |<br />
vēgavantaḥ plavantō yē plavagā mārutātmaja || 118 ||</p>
<p>tēṣāṁ mukhyatamaṁ manyē tvāmahaṁ kapikuñjara |<br />
atithiḥ kila pūjārhaḥ prākr̥tō:&#8217;pi vijānatā || 119 ||</p>
<p>dharmaṁ jijñāsamānēna kiṁ punastvādr̥śō mahān |<br />
tvaṁ hi dēvavariṣṭhasya mārutasya mahātmanaḥ || 120 ||</p>
<p>putrastasyaiva vēgēna sadr̥śaḥ kapikuñjara |<br />
pūjitē tvayi dharmajña pūjāṁ prāpnōti mārutaḥ || 121 ||</p>
<p>tasmāttvaṁ pūjanīyō mē śr̥ṇu cāpyatra kāraṇam |<br />
pūrvaṁ kr̥tayugē tāta parvatāḥ pakṣiṇō:&#8217;bhavan || 122 ||</p>
<p>tē:&#8217;bhijagmurdiśaḥ sarvā garuḍānilavēginaḥ | [tē hi]<br />
tatastēṣu prayātēṣu dēvasaṅghāḥ saharṣibhiḥ || 123 ||</p>
<p>bhūtāni ca bhayaṁ jagmustēṣāṁ patanaśaṅkayā |<br />
tataḥ kruddhaḥ sahasrākṣaḥ parvatānāṁ śatakratuḥ || 124 ||</p>
<p>pakṣāṁścicchēda vajrēṇa tatra tatra sahasraśaḥ |<br />
sa māmupāgataḥ kruddhō vajramudyamya dēvarāṭ || 125 ||</p>
<p>tatō:&#8217;haṁ sahasā kṣiptaḥ śvasanēna mahātmanā |<br />
asmim̐llavaṇatōyē ca prakṣiptaḥ plavagōttama || 126 ||</p>
<p>guptapakṣasamagraśca tava pitrā:&#8217;bhirakṣitaḥ |<br />
tatō:&#8217;haṁ mānayāmi tvāṁ mānyō hi mama mārutaḥ || 127 ||</p>
<p>tvayā mē hyēṣa sambandhaḥ kapimukhya mahāguṇaḥ |<br />
asminnēvaṁ gatē kāryē sāgarasya mamaiva ca || 128 [tasmin] ||</p>
<p>prītiṁ prītamanāḥ kartuṁ tvamarhasi mahākapē |<br />
śramaṁ mōkṣaya pūjāṁ ca gr̥hāṇa kapisattama || 129 ||</p>
<p>prītiṁ ca bahumanyasva prītō:&#8217;smi tava darśanāt |<br />
ēvamuktaḥ kapiśrēṣṭhastaṁ nagōttamamabravīt || 130 ||</p>
<p>prītō:&#8217;smi kr̥tamātithyaṁ manyurēṣō:&#8217;panīyatām |<br />
tvaratē kāryakālō mē ahaścāpyativartatē || 131 ||</p>
<p>pratijñā ca mayā dattā na sthātavyamihāntarē |<br />
ityuktvā pāṇinā śailamālabhya haripuṅgavaḥ || 132 ||</p>
<p>jagāmākāśamāviśya vīryavān prahasanniva |<br />
sa parvatasamudrābhyāṁ bahumānādavēkṣitaḥ || 133 ||</p>
<p>pūjitaścōpapannābhirāśīrbhiranilātmajaḥ |<br />
athōrdhvaṁ dūramutplutya hitvā śailamahārṇavau || 134 ||</p>
<p>pituḥ panthānamāsthāya jagāma vimalē:&#8217;mbarē |<br />
bhūyaścōrdhvagatiṁ prāpya giriṁ tamavalōkayan || 135 ||</p>
<p>vāyusūnurnirālambē jagāma vimalē:&#8217;mbarē |<br />
taddvitīyaṁ hanumatō dr̥ṣṭvā karma suduṣkaram || 136 ||</p>
<p>praśaśaṁsuḥ surāḥ sarvē siddhāśca paramarṣayaḥ |<br />
dēvatāścābhavan hr̥ṣṭāstatrasthāstasya karmaṇā || 137 ||</p>
<p>kāñcanasya sunābhasya sahasrākṣaśca vāsavaḥ |<br />
uvāca vacanaṁ dhīmānparitōṣātsagadgadam |<br />
sunābhaṁ parvataśrēṣṭhaṁ svayamēva śacīpatiḥ || 138 ||</p>
<p>hiraṇyanābha śailēndra parituṣṭō:&#8217;smi tē bhr̥śam |<br />
abhayaṁ tē prayacchāmi tiṣṭha saumya yathāsukham || 139 ||</p>
<p>sāhyaṁ kr̥taṁ tē sumahadvikrāntasya hanūmataḥ |<br />
kramatō yōjanaśataṁ nirbhayasya bhayē sati || 140 ||</p>
<p>rāmasyaiṣa hi dūtyēna yāti dāśarathērhariḥ |<br />
satkriyāṁ kurvatā tasya tōṣitō:&#8217;smi dr̥ḍhaṁ tvayā || 141 ||</p>
<p>tataḥ praharṣamagamadvipulaṁ parvatōttamaḥ |<br />
dēvatānāṁ patiṁ dr̥ṣṭvā parituṣṭaṁ śatakratum || 142 ||</p>
<p>sa vai dattavaraḥ śailō babhūvāvasthitastadā |<br />
hanūmāṁśca muhūrtēna vyaticakrāma sāgaram || 143 ||</p>
<p>tatō dēvāḥ sagandharvāḥ siddhāśca paramarṣayaḥ |<br />
abruvansūryasaṅkāśāṁ surasāṁ nāgamātaram || 144 ||</p>
<p>ayaṁ vātātmajaḥ śrīmānplavatē sāgarōpari |<br />
hanūmānnāma tasya tvaṁ muhūrtaṁ vighnamācara || 145 ||</p>
<p>rākṣasaṁ rūpamāsthāya sughōraṁ parvatōpamam |<br />
daṁṣṭrākarālaṁ piṅgākṣaṁ vaktraṁ kr̥tvā nabhaḥsamam || 146 ||</p>
<p>balamicchāmahē jñātuṁ bhūyaścāsya parākramam |<br />
tvāṁ vijēṣyatyupāyēna viṣādaṁ vā gamiṣyati || 147 ||</p>
<p>ēvamuktā tu sā dēvī daivatairabhisatkr̥tā |<br />
samudramadhyē surasā bibhratī rākṣasaṁ vapuḥ || 148 ||</p>
<p>vikr̥taṁ ca virūpaṁ ca sarvasya ca bhayāvaham |<br />
plavamānaṁ hanūmantamāvr̥tyēdamuvāca ha || 149 ||</p>
<p>mama bhakṣyaḥ pradiṣṭastvamīśvarairvānararṣabha |<br />
ahaṁ tvā bhakṣayiṣyāmi praviśēdaṁ mamānanam || 150 ||</p>
<p>ēvamuktaḥ surasayā prāñjalirvānararṣabhaḥ |<br />
prahr̥ṣṭavadanaḥ śrīmānidaṁ vacanamabravīt || 151 ||</p>
<p>rāmō dāśarathirnāma praviṣṭō daṇḍakāvanam |<br />
lakṣmaṇēna sahabhrātrā vaidēhyā cāpi bhāryayā || 152 ||</p>
<p>anyakāryaviṣaktasya baddhavairasya rākṣasaiḥ |<br />
tasya sītā hr̥tā bhāryā rāvaṇēna yaśasvinī || 153 ||</p>
<p>tasyāḥ sakāśaṁ dūtō:&#8217;haṁ gamiṣyē rāmaśāsanāt |<br />
kartumarhasi rāmasya sāhyaṁ viṣayavāsinī || 154 ||</p>
<p>athavā maithilīṁ dr̥ṣṭvā rāmaṁ cākliṣṭakāriṇam |<br />
āgamiṣyāmi tē vaktraṁ satyaṁ pratiśr̥ṇōmi tē || 155 ||</p>
<p>ēvamuktā hanumatā surasā kāmarūpiṇī |<br />
abravīnnātivartēnmāṁ kaścidēṣa varō mama || 156 ||</p>
<p>taṁ prayāntaṁ samudvīkṣya surasā vākyamabravīt |<br />
balaṁ jijñāsamānā vai nāgamātā hanūmataḥ || 157 ||</p>
<p>praviśya vadanaṁ mē:&#8217;dya gantavyaṁ vānarōttama |<br />
vara ēṣa purā dattō mama dhātrēti satvarā || 158 ||</p>
<p>vyādāya vaktraṁ vipulaṁ sthitā sā mārutēḥ puraḥ |<br />
ēvamuktaḥ surasayā kruddhō vānarapuṅgavaḥ || 159 ||</p>
<p>abravītkuru vai vaktraṁ yēna māṁ viṣahiṣyasē |<br />
ityuktvā surasāṁ kruddhō daśayōjanamāyataḥ || 160 ||</p>
<p>daśayōjanavistārō babhūva hanumāṁstadā |<br />
taṁ dr̥ṣṭvā mēghasaṅkāśaṁ daśayōjanamāyatam || 161 ||</p>
<p>cakāra surasāpyāsyaṁ viṁśadyōjanamāyatam |<br />
tāṁ dr̥ṣṭvā vistr̥tāsyāṁ tu vāyuputraḥ subuddhimān || 162 ||</p>
<p>hanūmāṁstu tataḥ kruddhastriṁśadyōjanamāyataḥ |<br />
cakāra surasā vaktraṁ catvāriṁśattathōcchritam || 163 ||</p>
<p>babhūva hanumānvīraḥ pañcāśadyōjanōcchritaḥ |<br />
cakāra surasā vaktraṁ ṣaṣṭiyōjanamāyatam || 164 ||</p>
<p>tathaiva hanumānvīraḥ saptatīyōjanōcchritaḥ |<br />
cakāra surasā vaktramaśītīyōjanāyatam || 165 ||</p>
<p>hanūmānacalaprakhyō navatīyōjanōcchritaḥ |<br />
cakāra surasā vaktraṁ śatayōjanamāyatam || 166 ||</p>
<p>taddr̥ṣṭvā vyāditaṁ tvāsyaṁ vāyuputraḥ subuddhimān |<br />
dīrghajihvaṁ surasayā sughōraṁ narakōpamam || 167 ||</p>
<p>sa saṅkṣipyātmanaḥ kāyaṁ jīmūta iva mārutiḥ |<br />
tasmin muhūrtē hanumān babhūvāṅguṣṭhamātrakaḥ || 168 ||</p>
<p>sō:&#8217;bhipatyāśu tadvaktraṁ niṣpatya ca mahājavaḥ |<br />
antarikṣē sthitaḥ śrīmānidaṁ vacanamabravīt || 169 ||</p>
<p>praviṣṭō:&#8217;smi hi tē vaktraṁ dākṣāyaṇi namō:&#8217;stu tē |<br />
gamiṣyē yatra vaidēhī satyaścāsīdvarastava || 170 ||</p>
<p>taṁ dr̥ṣṭvā vadanānmuktaṁ candraṁ rāhumukhādiva |<br />
abravītsurasā dēvī svēna rūpēṇa vānaram || 171 ||</p>
<p>arthasiddhyai hariśrēṣṭha gaccha saumya yathāsukham |<br />
samānayasva vaidēhīṁ rāghavēṇa mahātmanā || 172 ||</p>
<p>tattr̥tīyaṁ hanumatō dr̥ṣṭvā karma suduṣkaram |<br />
sādhu sādhviti bhūtāni praśaśaṁsustadā harim || 173 ||</p>
<p>sa sāgaramanādhr̥ṣyamabhyētya varuṇālayam |<br />
jagāmākāśamāviśya vēgēna garuḍōpamaḥ || 174 ||</p>
<p>sēvitē vāridhārābhiḥ patagaiśca niṣēvitē |<br />
caritē kaiśikācāryairairāvataniṣēvitē || 175 ||</p>
<p>siṁhakuñjaraśārdūlapatagōragavāhanaiḥ |<br />
vimānaiḥ sampatadbhiśca vimalaiḥ samalaṅkr̥tē |<br />
vajrāśanisamāghātaiḥ pāvakairupaśōbhitē || 176 ||</p>
<p>kr̥tapuṇyairmahābhāgaiḥ svargajidbhiralaṅkr̥tē |<br />
vahatā havyamatyarthaṁ sēvitē citrabhānunā || | 177 ||</p>
<p>grahanakṣatracandrārkatārāgaṇavibhūṣitē |<br />
maharṣigaṇagandharvanāgayakṣasamākulē || 178 ||</p>
<p>viviktē vimalē viśvē viśvāvasuniṣēvitē<br />
dēvarājagajākrāntē candrasūryapathē śivē || 179 ||</p>
<p>vitānē jīvalōkasya vitatē brahmanirmitē |<br />
bahuśaḥ sēvitē vīrairvidyādharagaṇairvaraiḥ || 180 ||</p>
<p>jagāma vāyumārgē tu garutmāniva mārutiḥ || 181<br />
[** adhikapāṭhaḥ &#8211;<br />
hanūmānmēghajālāni prākarṣanmārutō yathā ||<br />
kālāgarusavarṇāni raktapītasitāni ca |<br />
kapinā:&#8217;:&#8217;kr̥ṣyamāṇāni mahābhrāṇi cakāśirē ||<br />
praviśannabhrajālāni niṣpataṁśca punaḥ punaḥ |<br />
prāvr̥ṣīndurivābhāti niṣpatanpraviśaṁstadā ||<br />
**]</p>
<p>pradr̥śyamānaḥ sarvatra hanūmānmārutātmajaḥ |<br />
bhējē:&#8217;mbaraṁ nirālambaṁ lambapakṣa ivādrirāṭ || 182 ||</p>
<p>plavamānaṁ tu taṁ dr̥ṣṭvā siṁhikā nāma rākṣasī |<br />
manasā cintayāmāsa pravr̥ddhā kāmarūpiṇī || 183 ||</p>
<p>adya dīrghasya kālasya bhaviṣyāmyahamāśitā |<br />
idaṁ hi mē mahatsatvaṁ cirasya vaśamāgatam || 184 ||</p>
<p>iti sañcintya manasā chāyāmasya samākṣipat |<br />
chāyāyāṁ gr̥hyamāṇāyāṁ cintayāmāsa vānaraḥ || 185 ||</p>
<p>samākṣiptō:&#8217;smi sahasā paṅgūkr̥taparākramaḥ |<br />
pratilōmēna vātēna mahānauriva sāgarē || 186 ||</p>
<p>tiryagūrdhvamadhaścaiva vīkṣamāṇastataḥ kapiḥ |<br />
dadarśa sa mahatsatvamutthitaṁ lavaṇāmbhasi || 187 ||</p>
<p>taddr̥ṣṭvā cintayāmāsa mārutirvikr̥tānanam |<br />
kapirājēna kathitaṁ sattvamadbhutadarśanam || 188 ||</p>
<p>chāyāgrāhi mahāvīryaṁ tadidaṁ nātra saṁśayaḥ |<br />
sa tāṁ buddhvārthatattvēna siṁhikāṁ matimānkapiḥ || 189 ||</p>
<p>vyavardhata mahākāyaḥ prāvr̥ṣīva balāhakaḥ |<br />
tasya sā kāyamudvīkṣya vardhamānaṁ mahākapēḥ || 190 ||</p>
<p>vaktraṁ prasārayāmāsa pātālāntarasannibham |<br />
ghanarājīva garjantī vānaraṁ samabhidravat || 191 ||</p>
<p>sa dadarśa tatastasyā vivr̥taṁ sumahanmukham |<br />
kāyamātraṁ ca mēdhāvī marmāṇi ca mahākapiḥ || 192 ||</p>
<p>sa tasyā vivr̥tē vaktrē vajrasaṁhananaḥ kapiḥ |<br />
saṅkṣipya muhurātmānaṁ niṣpapāta mahābalaḥ || 193 ||</p>
<p>āsyē tasyā nimajjantaṁ dadr̥śuḥ siddhacāraṇāḥ |<br />
grasyamānaṁ yathā candraṁ pūrṇaṁ parvaṇi rāhuṇā || 194 ||</p>
<p>tatastasyā nakhaistīkṣṇairmarmāṇyutkr̥tya vānaraḥ |<br />
utpapātātha vēgēna manaḥ sampātavikramaḥ || 195 ||</p>
<p>tāṁ tu dr̥ṣṭyā ca dhr̥tyā ca dākṣiṇyēna nipātya hi | [ca]<br />
sa kapipravarō vēgādvavr̥dhē punarātmavān || 196 ||</p>
<p>hr̥tahr̥tsā hanumatā papāta vidhurāmbhasi |<br />
svayambhuvēva hanumān sr̥ṣṭastasyā nipātanē || 197 ||</p>
<p>tāṁ hatāṁ vānarēṇāśu patitāṁ vīkṣya siṁhikām |<br />
bhūtānyākāśacārīṇī tamūcuḥ plavagōttamam || 198 ||</p>
<p>bhīmamadya kr̥taṁ karma mahatsattvaṁ tvayā hatam |<br />
sādhayārthamabhiprētamariṣṭaṁ plavatāṁ vara || 199 ||</p>
<p>yasya tvētāni catvāri vānarēndra yathā tava |<br />
dhr̥tirdr̥ṣṭirmatirdākṣyaṁ sa karmasu na sīdati || 200 ||</p>
<p>sa taiḥ sambhāvitaḥ pūjyaḥ pratipannaprayōjanaḥ |<br />
jagāmākāśamāviśya pannagāśanavatkapiḥ || 201 ||</p>
<p>prāptabhūyiṣṭhapārastu sarvataḥ pratilōkayan |<br />
yōjanānāṁ śatasyāntē vanarājiṁ dadarśa saḥ || 202 ||</p>
<p>dadarśa ca patannēva vividhadrumabhūṣitam |<br />
dvīpaṁ śākhāmr̥gaśrēṣṭhō malayōpavanāni ca || 203 ||</p>
<p>sāgaraṁ sāgarānūpaṁ sāgarānūpajān drumān |<br />
sāgarasya ca patnīnāṁ mukhānyapi vilōkayan || 204 ||</p>
<p>sa mahāmēghasaṅkāśaṁ samīkṣyātmānamātmavān |<br />
nirundhantamivākāśaṁ cakāra matimānmatim || 205 ||</p>
<p>kāyavr̥ddhiṁ pravēgaṁ ca mama dr̥ṣṭvaiva rākṣasāḥ |<br />
mayi kautūhalaṁ kuryuriti mēnē mahākapiḥ || 206 ||</p>
<p>tataḥ śarīraṁ saṅkṣipya tanmahīdharasannibham |<br />
punaḥ prakr̥timāpēdē vītamōha ivātmavān || 207 ||</p>
<p>tadrūpamatisaṅkṣipya hanumān prakr̥tau sthitaḥ |<br />
trīn kramāniva vikramya balivīryaharō hariḥ || 208 ||</p>
<p>sa cārunānāvidharūpadhārī<br />
paraṁ samāsādya samudratīram |<br />
parairaśakyaḥ pratipannarūpaḥ<br />
samīkṣitātmā samavēkṣitārthaḥ || 209 ||</p>
<p>tataḥ sa lambasya girēḥ samr̥ddhē<br />
vicitrakūṭē nipapāta kūṭē |<br />
sakētakōddālakanārikēlē<br />
mahādrikūṭapratimō mahātmā || 210 ||</p>
<p>tatastu samprāpya samudratīraṁ<br />
samīkṣya laṅkāṁ girivaryamūrdhni |<br />
kapistu tasminnipapāta parvatē<br />
vidhūya rūpaṁ vyathayanmr̥gadvijān || 211 ||</p>
<p>sa sāgaraṁ dānavapannagāyutaṁ<br />
balēna vikramya mahōrmimālinam |<br />
nipatya tīrē ca mahōdadhēstadā<br />
dadarśa laṅkāmamarāvatīmiva || 212 ||</p>
